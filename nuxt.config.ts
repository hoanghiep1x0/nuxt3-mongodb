// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  nitro:{
    plugins:["~/server/index.ts"]
  },
  devtools: { enabled: true },
  runtimeConfig:{
    mongodbUri: "mongodb://root:131312htquynh@localhost:26017/siteTruyen?authSource=admin"
  }
})
