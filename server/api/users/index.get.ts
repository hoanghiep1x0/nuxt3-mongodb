import { User } from '../../models/users'
export default defineEventHandler(async (event) => {
    let users = await User.find({});
    return {
        users
    }
})