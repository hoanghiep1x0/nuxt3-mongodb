import { Schema, model } from "mongoose";


const userSchema = new Schema({
    email: {
        type: String
    },
    name: {
        type: String
    }
})

export const User = model('User', userSchema);